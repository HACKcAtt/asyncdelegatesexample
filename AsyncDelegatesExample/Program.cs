﻿using System;
using System.Threading;

namespace AsyncDelegatesExample
{
    // Вариант без асинхронности (закомментировано).
    // В процессе работы метод Main заблокирован покуда исполняется метод Display.
    // Вывод:
    //// Начинается работа метода Display....
    //// Завершается работа метода Display....
    //// Продолжается работа метода Main
    //// Результат равен 285
    //class Program
    //{
    //    public delegate int DisplayHandler();
    //    static void Main(string[] args)
    //    {
    //        DisplayHandler handler = new DisplayHandler(Display);
    //        int result = handler.Invoke();

    //        Console.WriteLine("Продолжается работа метода Main");
    //        Console.WriteLine("Результат равен {0}", result);

    //        Console.ReadLine();
    //    }

    //    static int Display()
    //    {
    //        Console.WriteLine("Начинается работа метода Display....");

    //        int result = 0;
    //        for (int i = 1; i < 10; i++)
    //        {
    //            result += i * i;
    //        }
    //        Thread.Sleep(3000);
    //        Console.WriteLine("Завершается работа метода Display....");
    //        return result;
    //    }
    //}
    
    // Вариант с асинхронностью.
    class Program
    {
        // Делегат.
        public delegate int DisplayHandler();
        static void Main(string[] args)
        {
            // Кладём в делегат ссылку на метод Display().
            DisplayHandler handler = new DisplayHandler(Display);
            // Теперь делегат вызывается в асинхронном режиме.
            IAsyncResult resultObj = handler.BeginInvoke(null, null);
            Console.WriteLine("Продолжается работа метода Main");
            // "Выключаем" делегат, когда это нужно, выводится время работы метода.
            int result = handler.EndInvoke(resultObj);
            Console.WriteLine("Результат равен {0}", result);

            Console.ReadLine();
        }
        // После вызова метода Display через выражение handler.BeginInvoke(null, null)
        // работа метода Main не приостанавливается.А выполнение метода Display через делегат
        // DisplayHandler происходит в другом потоке.И лишь когда выполнение в методе Main
        // дойдет до строки int result = handler.EndInvoke(resultObj); он блокируется и ожидает
        // завершения выполнения метода Display.

        static int Display()
        {
            Console.WriteLine("Начинается работа метода Display....");

            int result = 0;
            for (int i = 1; i < 10; i++)
            {
                result += i * i;
            }
            Thread.Sleep(3000);
            Console.WriteLine("Завершается работа метода Display....");
            return result;
        }
    }
}
